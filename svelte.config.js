import preprocess from "svelte-preprocess"
// import node from "@sveltejs/adapter-node"
import cloudflare from "@sveltejs/adapter-cloudflare"
// import vercel from "@sveltejs/adapter-vercel"

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess(),
  kit: {
    // adapter: vercel(),
    adapter: cloudflare(),
    target: "#svelte",
    serviceWorker: { files: (filepath) => !filepath.endsWith(".pdf") },
  },
}

export default config
