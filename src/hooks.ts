import type { Handle } from "@sveltejs/kit"

export const handle: Handle = async ({ request, resolve }) => {
  const { path } = request
  if (path === "/donate") {
    return { status: 307, headers: { Location: "https://secure.qgiv.com/for/rutgle/" } }
  } else if (path === "/blog") {
    return { status: 307, headers: { Location: "https://ruthsgleanings.medium.com/" } }
  } else if (path === "/about-us") {
    return { status: 307, headers: { Location: "/gleaning" } }
  } else if (path === "/partners") {
    return { status: 307, headers: { Location: "/gleaning#partners" } }
  } else if (path === "/food-donors") {
    return { status: 307, headers: { Location: "/gleaning#food-donors" } }
  } else if (path === "/volunteers") {
    return { status: 301, headers: { Location: "/volunteer" } }
  }

  return resolve(request)
}
