// Environment variables to be used in Svelte templates are defined here until https://github.com/sveltejs/kit/issues/720 is fixed

export const GITLAB_MERGE_REQUEST_ID = import.meta.env.VITE_GITLAB_MERGE_REQUEST_ID ?? ""
export const GITLAB_PROJECT_ID = import.meta.env.VITE_GITLAB_PROJECT_ID ?? ""
export const GITLAB_PROJECT_PATH = import.meta.env.VITE_GITLAB_PROJECT_PATH ?? ""
