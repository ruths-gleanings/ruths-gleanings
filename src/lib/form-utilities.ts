// progressively enhance a <form> that already works without JS
export function enhanceForm(
  form: HTMLFormElement,
  {
    handlePending,
    handleError,
    handleResult,
  }: {
    handlePending?: (data: FormData, form: HTMLFormElement) => void
    handleError?: (responseText: string | null, error: Error | null, form: HTMLFormElement) => void
    handleResult: <T>(result: T, form: HTMLFormElement) => void
  }
): { destroy(): void } {
  let currentToken: symbol

  async function handleSubmit(e: Event) {
    e.preventDefault()

    const token = (currentToken = Symbol())
    const body = new FormData(form)

    if (handlePending) {
      handlePending(body, form)
    }

    try {
      const response = await fetch(form.action, {
        method: form.method,
        headers: { accept: "application/json" },
        body,
      })

      if (token === currentToken) {
        if (response.ok) {
          handleResult(await response.json(), form)
        } else {
          const responseText = await response.text()
          if (handleError) {
            handleError(responseText, null, form)
          } else {
            console.error(responseText)
          }
        }
      }
    } catch (error) {
      if (handleError && error instanceof Error) {
        handleError(null, error, form)
      } else {
        throw error
      }
    }
  }

  form.addEventListener("submit", handleSubmit)

  return {
    destroy() {
      form.removeEventListener("submit", handleSubmit)
    },
  }
}

export enum FormFieldType {
  Name,
  Email,
  Phone,
  Address,
  ContactMethod,
  Affiliation,
  Involvement,
  Availability,
}
