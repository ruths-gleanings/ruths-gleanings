import { processForm } from "./_api"

export const post = processForm({
  firestoreCollection: "forms/volunteer/submissions",
  emailSubjectFormat: "{name} wants to volunteer!",
  emailTo: "volunteer@ruthsgleanings.com",
  redirectTo: "/volunteer",
})
