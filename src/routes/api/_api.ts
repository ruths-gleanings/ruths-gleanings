import type { RequestHandler } from "@sveltejs/kit"

export const processForm =
  (options: {
    firestoreCollection: string
    emailSubjectFormat: string
    emailTo: string
    redirectTo: string
  }): RequestHandler<unknown, FormData> =>
  async (request) => {
    // if the request came from a <form> submission (no JS), we'll want to
    //  redirect back to the corresponding page with the result in the URL
    const isHtmlSubmission = request.headers.accept !== "application/json"

    const body = Object.fromEntries(request.body) as { name: string; [key: string]: string }
    const isTest = body.name === "test"

    try {
      if (!isTest) {
        await saveToFirestore(options.firestoreCollection, body)
      }

      if (!import.meta.env.VITE_SENDGRID_API_KEY) {
        throw new Error("SendGrid API key not set")
      }

      await sendNotificationEmail(`${import.meta.env.VITE_SENDGRID_API_KEY}`, body, {
        subject: options.emailSubjectFormat.replace("{name}", body.name),
        to: isTest ? "mayfield@ruthsgleanings.com" : options.emailTo,
      })

      if (isHtmlSubmission) {
        return { status: 303, headers: { location: options.redirectTo + "?submitted" } }
      } else {
        return { status: 200 }
      }
    } catch (ex) {
      const error = ex instanceof Error ? ex.message : (ex as string)
      if (isHtmlSubmission) {
        return {
          status: 303,
          headers: { location: `${options.redirectTo}?submitted&error=${error}#submit` },
        }
      } else {
        return { status: 500, body: error }
      }
    }
  }

export const saveToFirestore = async (
  firestoreCollection: string,
  body: Record<string, string>
): Promise<unknown> => {
  const projectId = "ruths-gleanings"
  return fetch(
    `https://firestore.googleapis.com/v1/projects/${projectId}/databases/(default)/documents/${firestoreCollection}`,
    {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        fields: {
          ...Object.fromEntries(
            Object.entries(body).map(([key, value]) => [key, { stringValue: value }])
          ),
          timestamp: { timestampValue: new Date().toISOString() },
        },
      }),
    }
  ).then(async (it) => {
    if (it.ok) {
      return it.json()
    } else {
      console.error(await it.json())
      throw new Error("Error saving to database")
    }
  })
}

export const sendNotificationEmail = (
  apiKey: string,
  bodyObject: { name: string; [key: string]: string },
  emailInfo: {
    to: string
    subject: string
  }
): Promise<void> =>
  fetch("https://api.sendgrid.com/v3/mail/send", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${apiKey}`,
    },
    body: JSON.stringify({
      personalizations: [{ to: [{ email: emailInfo.to }] }],
      from: { name: "Ruth's Gleanings", email: "no-reply@ruthsgleanings.com" },
      subject: emailInfo.subject,
      content: [
        {
          type: "text/html",
          value: Object.entries(bodyObject)
            .map(
              ([key, value]) => `<p style="white-space: pre"><strong>${key}</strong>: ${value}</p>`
            )
            .join("\n"),
        },
      ],
      tracking_settings: {
        open_tracking: { enable: false },
        click_tracking: { enable: false },
      },
    }),
  }).then(async (it) => {
    if (!it.ok) {
      console.error(await it.json())
      throw new Error("Error sending notification email")
    }
  })
