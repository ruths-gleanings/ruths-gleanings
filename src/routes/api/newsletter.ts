import { processForm } from "./_api"

export const post = processForm({
  firestoreCollection: "forms/newsletter/subscriptions",
  emailSubjectFormat: "{name} subscribed to the mailing list!",
  emailTo: "newsletter@ruthsgleanings.com",
  redirectTo: "/",
})
