This folder contains an up-to-date export of the Google Maps FoodShare locations map

### How to export from Google Maps

1. Go to https://www.google.com/maps/d/drive?state=%7B%22ids%22%3A%5B%221-cvNiaQkbqpu3-meZYUszdUEs3iZqJWG%22%5D%2C%22action%22%3A%22open%22%2C%22userId%22%3A%22105792620613550233860%22%7D&usp=sharing
2. Click more options three-dots at the top of legend panel
3. "Export to KML/KMZ"
4. Use the following options:
   - Entire Map
   - [ ] Keep data up to date with network link KML (only usable online).
   - [x] Export as KML instead of KMZ. Does not support all icons.
5. Download KML to this directory

### How to import into Google Maps

1. Create empty new layer
2. Import
3. Select KML file
