This is a [SvelteKit](https://kit.svelte.dev/) project.

## Developing

Once you've installed dependencies with `yarn`, start a development server:

```bash
yarn dev

# or start the server and open the app in a new browser tab
yarn dev --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
yarn build
```

> You can preview the built app with `yarn preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

## Deploy on Vercel

[SvelteKit Vercel Adapter](https://github.com/sveltejs/kit/tree/master/packages/adapter-vercel)

### CI/CD

Tokens expire after 10 days of inactivity according to https://vercel.com/knowledge/why-is-vercel-cli-asking-me-to-log-in

1. Generate a token at https://vercel.com/account/tokens
2. Update VERCEL_TOKEN GitLab variable at https://gitlab.com/ruths-gleanings/ruths-gleanings/-/settings/ci_cd

## Third-party services

- Compress PDFs using "Basic compression" option - https://smallpdf.com/compress-pdf
- Host and transform images - https://cloudinary.com
- Google My Maps - https://mymaps.google.com
